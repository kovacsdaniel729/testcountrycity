//var countries;

$(function(){

	/*
		TODO:
		az oldal betöltésekor lekérni a /rest/countries URL-ről az országok listáját,
		és a város hozzáadása popup <select> elemébe berakni az <option>-öket
		OK
	*/

	$.get('/rest/countries',function(data){
		$select = $('#city-country');
		data.forEach(function(elem){
			$option = $('<option></option>').prop('value',elem.iso).text(elem.name);
			$select.append($option);
		})
	},
	'json');



	$('body').on('click', '#add-city-button', function(){
		//TODO: valamiért ez az eseménykezelő le sem fut... OK
		//TODO: összeszedni az űrlap adatait OK

		var cityData;

		cityData = {
			'name' : $('#city-name').val(),
			'population' : $('#city-population').val(),
			'country.iso' : $('#city-country').val()
		};
		//'country' : {'iso' : $('#city-country').val() }

		$.ajax({
			url: '/rest/cities',
			method : 'PUT',
			data : cityData,
			dataType : 'json',
			complete : function(xhr, status){
					//TODO: Be kellene csukni a popupot OK
					$('#add-city-modal').modal('hide');
					console.log(status);
			}
		});


	});

		//TODO: ország törlés gombok működjenek OK
	$('table').on('click','.delete-country',function(){
		var iso = $(this).attr("data");
		var $this = $(this);

		$.ajax({
			url: '/rest/countries/'+iso,
			method : 'DELETE',
			success : function(data, status, xhr){
				console.log("ajax DELETE:  iso = " + iso + ', response: ' + data);
				$this.closest('tr').remove();
			}
		});

	})

		//TODO: város törlés gombok működjenek OK

	$('table').on('click','.delete-city',function(){
		var id = $(this).attr("data");
		var $this = $(this);

		$.ajax({
			url: '/rest/cities/'+id,
			method : 'DELETE',
			success : function(data, status, xhr){
				console.log("ajax DELETE:  id = " + id + ', response: ' + data);
				$this.closest('tr').remove();
			}
		});

	})


});
